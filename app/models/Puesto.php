<?php
namespace App\Models;

use PDO;
use Core\Model;

class Puesto extends Model
{

    function __construct() {}

        // Devolvemos la lista de todos los puestos que hay en la base de datos
        public static function all()
        {
            // Instancia de la BD
            $db = Puesto::db();
            
            // Consulta que vamos a realizar
            $statement = $db->query("SELECT * FROM puestos");
            // Realiazamos la consulta y lo parseamos a objetos de clase "Puesto"
            $puestos = $statement->fetchAll(PDO::FETCH_CLASS,Puesto::class);
            // Devolvemos todo
            return $puestos;
        }
}
