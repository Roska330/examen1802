<?php
namespace App\Models;

use PDO;
use Core\Model;

class Jugador extends Model
{

    function __construct()
    { 
        // Creamos una variable de clase para mostar la fecha de nacimiento en un formato más amigable
        $this->fecha = new \DateTime($this->nacimiento);
    }

    // Devolvemos la lista de todos los puestos que hay en la base de datos
    public static function all()
    {
        // Instancia de la BD
        $db = Jugador::db();
        // Consulta que vamos a realizar
        $statement = $db->query("SELECT * FROM jugadores");
        // Realiazamos la consulta y lo parseamos a objetos de clase "Puesto"
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS,Jugador::class);
        // Devolvemos todo
        return $jugadores;
    }

    // Insertarmos un nuevo jugador
    public function insert()
    {
        // Instancia de la BD
        $db = Jugador::db();
        // Consulta que vamos a realizar
        $statement = $db->prepare("INSERT INTO jugadores(id_puesto,nacimiento,nombre) VALUES (:id_puesto,:nacimiento,:nombre)");
        // Recogemos los valores y los bindeamos a la consulta
        $statement->bindValue(":id_puesto", $this->id_puesto);
        $statement->bindValue(":nacimiento", $this->nacimiento);
        $statement->bindValue(":nombre", $this->nombre);
        // Ejecutamos la consulta (El return esta for fun)
        return $statement->execute();
    }

    // Buscamos un jugador con el ID que recibimos
    public static function buscar($id){
        // Instancia de la BD
        $db = Jugador::db();
        // Consulta que vamos a realizar
        $statement = $db->prepare("SELECT * FROM jugadores WHERE id = :id");
        // Ejecutamos la consulta
        $statement->execute(array(":id" => $id));
        // Parseamos la consulta en una clase de tipo "Jugador"
        $statement->setFetchMode(PDO::FETCH_CLASS,Jugador::class);
        $jugador = $statement->fetch(PDO::FETCH_CLASS);
        // Devolvemos el jugador
        return $jugador;
    }  

    // Consulta que nos devuelve los jugadores de manera paginada
    public function paginate($length = 5)
    {
        // Comprobamos si estamos en alguna página
        if(isset($_REQUEST["page"])) {
            $page = (int) $_REQUEST["page"];
        } else {
            $page = 1;
        }
        // Calculamos el primer registro
        $offset = ($page - 1) * $length;
        // Instancia de la BD
        $db = Jugador::db();
        // Consulta
        $statement = $db->prepare("SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset");
        // Registros por pagina
        $statement->bindValue(":pagesize", $length,PDO::PARAM_INT);
        // Primer registro de la pagina
        $statement->bindValue(":offset", $offset,PDO::PARAM_INT);
        // Ejecutamos consulta
        $statement->execute();
        // Convertimos los datos devueltos por la consulta en una lista de clase "Jugador"
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS,Jugador::class);
        // Devolvemos los jugadores
        return $jugadores;
    }

    // Consulta que nos devuelve las páginas que tendremos de jugadores
    public static function rowCount()
    {
        $db = Jugador::db();
        $statement = $db->prepare("SELECT count(id) as count FROM jugadores");
        $statement->execute();
        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount["count"];
    }

    // Buscamos el puesto que tiene un jugador
    public function puesto()
    {        
        $db = Puesto::db();
        $statement = $db->prepare('SELECT * FROM puestos WHERE id = :id');
        $statement->bindValue(':id', $this->id_puesto);
        $statement->execute();
        $puesto = $statement->fetchAll(PDO::FETCH_CLASS, Puesto::class)[0];

        return $puesto;
    }

    // Método que se lanza al intentar ejecutar un metodo sin utilizar los "()"
    public function __get($metodo)
    {
        if (method_exists($this, $metodo)) {
            $this->$metodo = $this->$metodo();
            return $this->$metodo;            
        } else {
            return "";
        }
    }

}
