<?php
namespace App\Controllers;
use \App\Models\Jugador;
use \App\Models\Puesto;

class JugadorController
{

    function __construct(){}


    public function index()
    {
        // Jugadores
        $jugadores = Jugador::paginate(5);
        // Numero de filas
        $rowCount = Jugador::rowCount();

        // Páginas 
        $pages = ceil($rowCount / 5);
        // Comprobamos si tenemos alguna pagina guardada
        if(isset($_REQUEST["page"])) {
            $page = (int) $_REQUEST["page"];
        } else {
            $page = 1;
        }
        // Cargamos la página index
        require "../app/views/jugador/index.php";
    }

    // Método crear
    public function crear()
    {
        // Cargamos los puestos existentes en la BBDD
        $puestos = Puesto::all();
        // Cargamos la página de registro
        require "../app/views/jugador/registrar.php";
    }

    public function insertar()
    {
        // Creamos un nuevo jugador
        $jugador = new Jugador();
        $jugador->nombre = $_REQUEST["nombre"];
        $jugador->nacimiento = $_REQUEST["nacimiento"];
        $jugador->id_puesto = $_REQUEST["id_puesto"];  
        // Lo insertamos      
        $jugador->insert();
        // Redirigimos a la página principal de jugador
        header("Location:/jugador");         
    }

    // Añadimos jugador a titular
    public function titular($args) 
    {
        // Creamos un array
        $titulares = [];
        // Recogemos el ID del jugador a agregar
        $id = (int)$args[0];
        $jugador = Jugador::buscar($id);

        // Comprobamos si ya esta en el equipo titular
        if(!array_key_exists($id,$titulares)) {
            // Miramos si tenemos algun titular ya agregado
            if(isset($_SESSION["titulares"])) {
                $titulares = $_SESSION["titulares"];
            }
            // Agregamos el nuevo titular
            $titulares[$id] = $jugador;
            // Reescribimos el array de sesión
            $_SESSION["titulares"] = $titulares;            
        }
        // Cargamos la página de titulares
        require "../app/views/jugador/titulares.php";

    }
    public function quitar($args) 
    {
        // Creamos un array
        $titulares = [];
        // Recogemos el ID del jugador que queremos echar al banquillo
        $id = (int)$args[0];

        // Comprobamos si tenemos titulares ( Por si nos escriben la ruta directamente en la URL)
        if(isset($_SESSION["titulares"])) {
            // Cargamos los titulares en el array
            $titulares = $_SESSION["titulares"];
            // Comprobamos si esta el jugador en los titulares
            if(array_key_exists($id,$titulares)) {
                // Lo quitamos
                unset($titulares[$id]);
            }
            // Reescribimos el array de sesión
            $_SESSION["titulares"] = $titulares;        
        }
        // Redirigimos a la página de titulares
        header("Location:/jugador/titulares");
    }

    // Método que nos cargara la página con los titulares
    public function titulares()
    {        
        require "../app/views/jugador/titulares.php";
    }


   

}
