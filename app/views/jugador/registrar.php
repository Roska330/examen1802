<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">    
        <h1>Nuevo jugador</h1>

        <form action="/jugador/insertar" method="post">

          <div class="form-group">
            <label>Nombre:</label>
            <input type="text" class="form-control" name="nombre" placeholder="Introduzca su nombre...">
        </div>
        <div class="form-group">
            <label>Fecha de nacimiento:</label>
            <input type="datetime-local" class="form-control" name="nacimiento" placeholder="1996-05-18" min="1990-01-01" max="2018-11-28">
        </div>

       <div class="form-group">
            <label>Puesto:</label>
            <select name="id_puesto" class="form-control">
                <?php foreach ($puestos as $puesto): ?>
                    <option value="<?php echo $puesto->id ?>"><?php echo $puesto->nombre ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <button type="submit" class="btn btn-default">Crear jugador</button>

    </form>

    </div>

  </main>
  <?php require "../app/views/parts/footer.php" ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
