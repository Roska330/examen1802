<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>
  <main role="main" class="container">
    <div class="starter-template">
        <!-- Comprobamos que ha introducido todos los campos -->  
        <h1 style="color: red"><?php echo isset($_SESSION["error"]) ? $_SESSION["error"] : ""  ?></h1>
        <!-- Después de mostrar el error lo quitamos -->  
        <?php if (isset($_SESSION["error"])): ?>
            <!--<?php unset($_SESSION['error']) ?>-->
        <?php endif ?>
        
        <h1>Titulares</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Puesto</th>
                    <th>Fecha de nacimiento</th>
                    <th>Operaciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($_SESSION["titulares"] as $titular): ?>
                <tr>
                    <td><?php echo $titular->id ?></td>
                    <td><?php echo $titular->nombre ?></td>
                    <td><?php echo $titular->puesto->nombre ?></td>                    
                    <td><?php echo $titular->fecha->format("d-m-Y") ?></td>
                    <td>
                        <a class="btn btn-primary" href="/jugador/quitar/<?php echo $titular->id ?>">Quitar</a>
                    </td>
                </tr>
                <?php endforeach ?>

            </tbody>
        </table>    

</main>
<?php require "../app/views/parts/footer.php" ?>

</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
